# Foo-Car Rental #

Foo-Car Rental is a dummy car rental service created as an example project.

## Installation ##
* clone repository
* run composer install
* create database for application
* import .sql files in app/sql
* copy config.php from app/config/dev or app/config/prd to app/config
* edit config.php in app/config to match local environment settings
* set virtualhost root to public/
* have fun!

\- Tijs