<?php

use FooCar\Entity\Request;
use FooCar\Usecase\AddBooking;
use PHPUnit\Framework\TestCase;

class AddBookingTest extends TestCase {

    private $request;

    public function setUp() {
        $this->request = new Request();
    }

    /** @test */
    public function bookingRequestMissingName_returnsError() {
        $this->request->name = '';
        $this->request->email = 'email';
        $this->request->dateFrom = 'date from';
        $this->request->dateTo = 'date to';

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('missing name in request', $response->message);
    }

    /** @test */
    public function bookingRequestMissingEmail_returnsError() {
        $this->request->name = 'name';
        $this->request->email = '';
        $this->request->dateFrom = 'date from';
        $this->request->dateTo = 'date to';

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('missing email in request', $response->message);
    }

    /** @test */
    public function bookingRequestMissingDateFrom_returnsError() {
        $this->request->name = 'name';
        $this->request->email = 'email';
        $this->request->dateFrom = '';
        $this->request->dateTo = 'date to';

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('missing date from in request', $response->message);
    }

    /** @test */
    public function bookingRequestMissingDateTo_returnsError() {
        $this->request->name = 'name';
        $this->request->email = 'email';
        $this->request->dateFrom = 'date from';
        $this->request->dateTo = '';

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('missing date to in request', $response->message);
    }

    /** @test */
    public function dateFromFallsAfterDateTo_returnsError() {
        $this->request->name = 'name';
        $this->request->email = 'email';
        $this->request->dateFrom = date('Ymd', strtotime('-1 day'));
        $this->request->dateTo = date('Ymd', strtotime('-2 days'));

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('date from date falls after date to', $response->message);
    }

    /* add additional tests to check request data integrity... */

    /** @test */
    public function minimumAmountOfDaysBetweenDatesLessThen3Days_returnsError() {
        $this->request->name = 'name';
        $this->request->email = 'email';
        $this->request->dateFrom = date('Ymd', strtotime('+1 day'));
        $this->request->dateTo = date('Ymd', strtotime('+3 days'));

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('time between dates is less than minimum of 3 days', $response->message);
    }

    /** @test */
    public function minimumAmountOfDaysBetweenDatesMoreThen14Days_returnsError() {
        $this->request->name = 'name';
        $this->request->email = 'email';
        $this->request->dateFrom = date('Ymd', strtotime('+1 day'));
        $this->request->dateTo = date('Ymd', strtotime('+16 days'));

        $booker = new AddBooking();
        $booker->setRequest($this->request);
        $booker->book();

        $response = $booker->getResponse();

        $this->assertFalse($response->success);
        $this->assertEquals('time between dates is more than maximum of 14 days', $response->message);
    }

}