<?php

require_once __DIR__ . '/../vendor/autoload.php';

use FooCar\App;

$app = new App(require_once __DIR__ . '/../app/config/config.php');

$app->run();