<?php

namespace FooCar\Usecase;

use FooCar\App;
use FooCar\Database\DatabaseBooking;
use FooCar\Database\DatabaseCar;
use FooCar\Database\DatabaseCustomer;
use FooCar\Entity\Customer;
use FooCar\Entity\Response;

class AddBooking {

    private $request;
    private $response;

    private $databaseBooking;
    private $databaseCar;

    /* add offsets to config in stead of as constants */
    const DATE_MINIMUM_OFFSET = '+3 days';
    const DATE_MAXIMUM_OFFSET = '+14 days';
    const SECONDS_IN_A_DAY = 86400;

    const CAR_MAX_NUMBER_OF_PEOPLE = 7;

    public function setRequest($request) {
        $this->request = $request;
    }

    public function book() {
        $this->response = new Response();

        if(empty($this->request->name)) {
            $this->response->success = false;
            $this->response->message = 'missing name in request';
        } elseif(empty($this->request->email)) {
            $this->response->success = false;
            $this->response->message = 'missing email in request';
        } elseif(empty($this->request->dateFrom)) {
            $this->response->success = false;
            $this->response->message = 'missing date from in request';
        } elseif(empty($this->request->dateTo)) {
            $this->response->success = false;
            $this->response->message = 'missing date to in request';
        } elseif(strtotime($this->request->dateFrom) > strtotime($this->request->dateTo)) {
            $this->response->success = false;
            $this->response->message = 'date from date falls after date to';
        } elseif($this->checkPeriodFallsWithinOffsets($this->request) && $this->request->numberOfPeople <= self::CAR_MAX_NUMBER_OF_PEOPLE) {
            /* TODO: create separate usecases for Car/Customer processes */
            $this->databaseBooking = new DatabaseBooking(App::$databaseConfig);
            $this->databaseCar = new DatabaseCar(App::$databaseConfig);

            $findCustomer = new FindCustomer();
            $findCustomer->setRequest($this->request);
            $findCustomer->execute();

            /** @var $customer Customer */
            $customer = $findCustomer->getResponse()->customer;

            $allCars = $this->databaseCar->getAllCars();

            $numberOfDays = ( strtotime($this->request->dateTo) - strtotime($this->request->dateFrom) ) / self::SECONDS_IN_A_DAY;

            $carAvailable = false;
            for($i = 0; $i < count($allCars); $i++) {
                if($this->checkAvailability($numberOfDays, $allCars[$i]['id']) && $allCars[$i]['seats'] >= $this->request->numberOfPeople) {
                    $carAvailable = true;
                    $availableCarId = $allCars[$i]['id'];
                    break;
                } else {
                    $carAvailable = false;
                }
            }

            if($carAvailable) {
                for($i = 0; $i < $numberOfDays; $i ++) {
                    $this->databaseBooking->addBooking(date('Ymd', strtotime($this->request->dateFrom . ' +' . $i . ' days')), $availableCarId, $customer->id);
                }
                $this->response->success = true;
                $this->response->booking = $this->request;
                $this->response->message = 'Booking confirmed! Your confirmation will be sent to ' . $this->request->email;

            } else {
                $this->response->success = false;
                $this->response->message = 'Car booked for (part of) requested period';
            }
        }
    }

    public function getResponse() {
        return $this->response;
    }

    private function checkPeriodFallsWithinOffsets($request) {
        $dateFromMinimumOffset = date('Ymd', strtotime($request->dateFrom . self::DATE_MINIMUM_OFFSET));
        $dateFromMaximumOffset = date('Ymd', strtotime($request->dateFrom . self::DATE_MAXIMUM_OFFSET));

        if(strtotime($request->dateTo) < strtotime($dateFromMinimumOffset)) {
            $this->response->success = false;
            $this->response->message = 'time between dates is less than minimum of 3 days';

            return false;
        }
        if(strtotime($request->dateTo) > strtotime($dateFromMaximumOffset)) {
            $this->response->success = false;
            $this->response->message = 'time between dates is more than maximum of 14 days';

            return false;
        }

        return true;
    }

    private function checkAvailability($numberOfDays, $carId) {
        for($i = 0; $i < $numberOfDays; $i ++) {
            $isBooked = $this->databaseBooking->checkBooking(date('Ymd', strtotime($this->request->dateFrom . ' +' . $i . ' days')), $carId);
            if($isBooked) {
                return false;
            } else {
                return true;
            }
        }
    }
}