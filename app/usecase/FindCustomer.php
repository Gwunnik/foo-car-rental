<?php
/**
 * Created by PhpStorm.
 * User: Tijs van Raaij
 * Date: 03/21/2017
 * Time: 22:06
 */

namespace FooCar\Usecase;

use FooCar\App;
use FooCar\Database\DatabaseCustomer;
use FooCar\Entity\Customer;
use FooCar\Entity\Response;

class FindCustomer {

    private $request;
    private $response;

    public function setRequest($request) {
        $this->request = $request;
    }

    public function execute() {
        $this->response = new Response();
        $databaseCustomer = new DatabaseCustomer(App::$databaseConfig);

        $customer = new Customer();
        $customer->email = $this->request->email;
        $customer->name = $this->request->name;

        if($databaseCustomer->getCustomerByEmail($this->request->email)) {
            $result = $databaseCustomer->getCustomerByEmail($this->request->email);
            $customer->id = $result['id'];
        } else {
            $result = $databaseCustomer->save($customer);
            $customer->id = $result['id'];
        }

        $this->response->customer = $customer;
        $this->response->success = true;
    }

    public function getResponse() {
        return $this->response;
    }

}
