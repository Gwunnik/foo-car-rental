<?php

namespace FooCar\Database;

use PDO;

class Database {

    protected $connection;
    protected $config;

    public function __construct($config) {
        $this->initializeConnection($config);
    }

    private function initializeConnection($config) {
        $host = $config['host'];
        $username = $config['username'];
        $password = $config['password'];
        $database = $config['database'];

        try {
            $this->connection = new PDO('mysql:host=' . $host . ';dbname=' . $database, $username,
                $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "<html><body>";
            echo 'Something went wrong when connecting to database: ' . $e->getMessage();
            echo "</body></html>";
            exit();
        }
    }

}