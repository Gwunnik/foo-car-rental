<?php

namespace FooCar\Database;

use PDO;

class DatabaseCar extends Database {

    public function __construct($config) {
        parent::__construct($config);
    }

    public function getAllCars() {
        $sql = $this->connection->prepare('SELECT * FROM car');
        $sql->execute();

        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}