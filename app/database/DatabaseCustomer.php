<?php

namespace FooCar\Database;

use PDO;

class DatabaseCustomer extends Database {

    public function __construct($config) {
        parent::__construct($config);
    }

    public function getCustomerByEmail($email) {
        $sql = $this->connection->prepare('SELECT id FROM customer WHERE email = :email');
        $sql->bindParam(':email', $email);
        $sql->execute();

        $result = $sql->fetch(PDO::FETCH_ASSOC);

        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function save($customer) {
        $sql = $this->connection->prepare('INSERT INTO customer(name, email) VALUES (:name, :email)');

        $bookingParameters = array(
            'name' => $customer->name,
            'email' => $customer->email,
        );

        if($sql->execute($bookingParameters)) {
            return $this->connection->lastInsertId();
        } else {
            return false;
        }
    }
}