<?php

namespace FooCar\Database;

use PDO;

class DatabaseBooking extends Database {

    public function __construct($config) {
        parent::__construct($config);
    }


    public function addBooking($date, $availableCarId, $customerId) {
        $sql = $this->connection->prepare('INSERT INTO booking(car_id, date, customer_id) VALUES (:car_id, :date, :customer_id)');

        $bookingParameters = array(
            'car_id' => $availableCarId,
            'date' => $date,
            'customer_id' => $customerId,
        );

        if($sql->execute($bookingParameters)) {
            return true;
        } else {
            return false;
        }
    }

    public function checkBooking($date, $carId) {
        $sql = $this->connection->prepare('SELECT * FROM booking WHERE car_id=' . $carId . ' AND date=' . $date);

        $sql->execute();
        $result = $sql->fetch(PDO::FETCH_ASSOC);

        if(empty($result)) {
            return false;
        } else {
            return $result['date'];
        }

    }

}