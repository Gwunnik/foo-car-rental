<?php

namespace FooCar\Presenter;

use Twig_Environment;
use Twig_Loader_Filesystem;

abstract class Presenter {

    protected $twig;
    protected $response;

    public function __construct() {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../template');
        $this->twig = new Twig_Environment($loader, array( 'cache' => __DIR__ . '/../tmp/cache' ));
    }

    public function setResponse($response) {
    }

    public function present() {
    }

}