<?php

namespace FooCar\Presenter;

use FooCar\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class AddBookingPresenter extends Presenter {

    public function setResponse($response) {
        $this->response = $response;
    }

    public function present() {
        if($this->response) {
            if($this->response->success) {
                /* TODO: disconnect mailer from presenter */
                $transport = Swift_SmtpTransport::newInstance(App::$mailConfig['host'], App::$mailConfig['port']);
                if(isset(App::$mailConfig['username'])) {
                    $transport->setUsername(App::$mailConfig['username']);
                }
                if(isset(App::$mailConfig['password'])) {
                    $transport->setPassword(App::$mailConfig['password']);
                }

                $mailer = Swift_Mailer::newInstance($transport);

                $emailMessage = Swift_Message::newInstance();
                $emailMessage->setSubject('Congratulations! You have booked a Foo-Car Rental Car');
                $emailMessage->setFrom('no-reply@foocar.tijsvanraaij.nl');
                $emailMessage->setTo($this->response->booking->email);
                $emailMessage->setBcc(array( 'bookings@foocar.tijsvanraaij.nl', 'tijs.vanraaij@gmail.com' ));
                $emailMessage->setBody($this->twig->render(
                    'email/addBooking.twig',
                    array(
                        'name' => $this->response->booking->name,
                        'dateFrom' => $this->response->booking->dateFrom,
                        'dateTo' => $this->response->booking->dateTo,
                        'numberOfPeople' => $this->response->booking->numberOfPeople,
                    )
                ),
                    'text/html');

                $mailer->send($emailMessage);
            }
            $this->twig->display('addBooking.twig', array('response' => $this->response));
        } else {
            $this->twig->display('addBooking.twig');
        }

    }
}