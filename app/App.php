<?php

namespace FooCar;

use FooCar\Database\Database;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class App {

    public static $databaseConfig;
    public static $mailConfig;

    private $collection;

    public function __construct($config) {
        self::$databaseConfig = $config['database'];
        self::$mailConfig = $config['mail'];

        $this->router = new Router();
        /* TODO: put routes somewhere else */
        $this->collection = new RouteCollection();
        $this->collection->add('/', new Route('/', array('controller' => '\FooCar\Controller\BookingController')));
        $this->collection->add('addBooking', new Route('addBooking', array('controller' => '\FooCar\Controller\BookingController', 'parameters' => $_POST)));
    }

    public function run() {
        $context = new RequestContext('/');
        if(isset($_REQUEST['path'])) {
            $context->setPathInfo($_REQUEST['path']);
        }

        $matcher = new UrlMatcher($this->collection, $context);
        $routingRequest = $matcher->match($context->getBaseUrl() . $context->getPathInfo());

        $this->router->setRequest($routingRequest);
        $this->router->execute();
    }

}