<?php

namespace FooCar;

class Router {

    private $request;

    public function setRequest($request) {
        $this->request = $request;
    }

    public function execute() {
        $controller = new $this->request['controller']();

        $controller->setRequest($this->request);
        $controller->execute();
    }

}