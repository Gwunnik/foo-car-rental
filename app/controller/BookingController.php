<?php

namespace FooCar\Controller;

use FooCar\Entity\Request;
use FooCar\Entity\Response;
use FooCar\Presenter\AddBookingPresenter;
use FooCar\Presenter\Presenter;
use FooCar\Usecase\AddBooking;

class BookingController {

    private $request;

    /** @var $presenter Presenter*/
    private $presenter;

    public function setRequest($request) {
        $this->request = $request;

        $this->presenter = new AddBookingPresenter();
    }

    public function execute() {
        $response = new Response();

        if(isset($this->request['parameters'])) {
            $request = new Request();
            $request->name = $this->request['parameters']['name'];
            $request->email = $this->request['parameters']['email'];
            $request->numberOfPeople = $this->request['parameters']['numberOfPeople'];
            $request->dateFrom = $this->request['parameters']['dateFrom'];
            $request->dateTo = $this->request['parameters']['dateTo'];

            $addBooking = new AddBooking();
            $addBooking->setRequest($request);
            $addBooking->book();

            $response = $addBooking->getResponse();
        }
        $this->presenter->setResponse($response);
        $this->presenter->present();
    }

}