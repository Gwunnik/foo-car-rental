<?php

return array(
    'database' => array(
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'database' => 'foocar',
    ),
    'mail' => array(
        'host' => '127.0.0.1',
        'port' => '1025',
        'username' => '',
        'password' => '',
    )
);